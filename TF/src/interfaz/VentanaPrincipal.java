package interfaz;

import dominio.*;
import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class VentanaPrincipal extends javax.swing.JFrame {

    private Arbol tree;
    private boolean hijo;
    private boolean union;
    private boolean padre;
    private boolean madre;
    private int indxImg;
    private int indxDoc;
    private boolean img;
    private boolean doc;
    private String[] paises;

    public VentanaPrincipal() {
        initComponents();
        this.setResizable(false);
        tree = new Arbol();
        hijo = false;
        union = false;
        padre = false;
        img = false;
        doc = false;
        indxImg = 0;
        indxDoc = 0;
        cargarDatos(tree);
        cargarCB();
        actualizar();
        panelPrincipal.removeAll();
        panelMenuGrafico.show();
        panelPrincipal.add(panelArbol);
        panelPrincipal.repaint();
        panelPrincipal.revalidate();
        cbSelectEvent();
        txtAreaArbGrafico.setEditable(false);
        labelRef.setIcon(new ImageIcon("src/img/referencias.png"));
        cargarPaises();
    }

    private void cargarPaises() {
        String paisesAux = "Afganistán\n"
                + "Albania\n"
                + "Alemania\n"
                + "Andorra\n"
                + "Angola\n"
                + "Antigua y Barbuda\n"
                + "Arabia Saudita\n"
                + "Argelia\n"
                + "Argentina\n"
                + "Armenia\n"
                + "Australia\n"
                + "Austria\n"
                + "Azerbaiyán\n"
                + "Bahamas\n"
                + "Bangladés\n"
                + "Barbados\n"
                + "Baréin\n"
                + "Bélgica\n"
                + "Belice\n"
                + "Benín\n"
                + "Bielorrusia\n"
                + "Birmania\n"
                + "Bolivia\n"
                + "Bosnia y Herzegovina\n"
                + "Botsuana\n"
                + "Brasil\n"
                + "Brunéi\n"
                + "Bulgaria\n"
                + "Burkina Faso\n"
                + "Burundi\n"
                + "Bután\n"
                + "Cabo Verde\n"
                + "Camboya\n"
                + "Camerún\n"
                + "Canadá\n"
                + "Catar\n"
                + "Chad\n"
                + "Chile\n"
                + "China\n"
                + "Chipre\n"
                + "Ciudad del Vaticano\n"
                + "Colombia\n"
                + "Comoras\n"
                + "Corea del Norte\n"
                + "Corea del Sur\n"
                + "Costa de Marfil\n"
                + "Costa Rica\n"
                + "Croacia\n"
                + "Cuba\n"
                + "Dinamarca\n"
                + "Dominica\n"
                + "Ecuador\n"
                + "Egipto\n"
                + "El Salvador\n"
                + "Emiratos Árabes Unidos\n"
                + "Eritrea\n"
                + "Eslovaquia\n"
                + "Eslovenia\n"
                + "España\n"
                + "Estados Unidos\n"
                + "Estonia\n"
                + "Etiopía\n"
                + "Filipinas\n"
                + "Finlandia\n"
                + "Fiyi\n"
                + "Francia\n"
                + "Gabón\n"
                + "Gambia\n"
                + "Georgia\n"
                + "Ghana\n"
                + "Granada\n"
                + "Grecia\n"
                + "Guatemala\n"
                + "Guyana\n"
                + "Guinea\n"
                + "Guinea ecuatorial\n"
                + "Guinea-Bisáu\n"
                + "Haití\n"
                + "Honduras\n"
                + "Hungría\n"
                + "India\n"
                + "Indonesia\n"
                + "Irak\n"
                + "Irán\n"
                + "Irlanda\n"
                + "Islandia\n"
                + "Islas Marshall\n"
                + "Islas Salomón\n"
                + "Israel\n"
                + "Italia\n"
                + "Jamaica\n"
                + "Japón\n"
                + "Jordania\n"
                + "Kazajistán\n"
                + "Kenia\n"
                + "Kirguistán\n"
                + "Kiribati\n"
                + "Kuwait\n"
                + "Laos\n"
                + "Lesoto\n"
                + "Letonia\n"
                + "Líbano\n"
                + "Liberia\n"
                + "Libia\n"
                + "Liechtenstein\n"
                + "Lituania\n"
                + "Luxemburgo\n"
                + "Madagascar\n"
                + "Malasia\n"
                + "Malaui\n"
                + "Maldivas\n"
                + "Malí\n"
                + "Malta\n"
                + "Marruecos\n"
                + "Mauricio\n"
                + "Mauritania\n"
                + "México\n"
                + "Micronesia\n"
                + "Moldavia\n"
                + "Mónaco\n"
                + "Mongolia\n"
                + "Montenegro\n"
                + "Mozambique\n"
                + "Namibia\n"
                + "Nauru\n"
                + "Nepal\n"
                + "Nicaragua\n"
                + "Níger\n"
                + "Nigeria\n"
                + "Noruega\n"
                + "Nueva Zelanda\n"
                + "Omán\n"
                + "Países Bajos\n"
                + "Pakistán\n"
                + "Palaos\n"
                + "Panamá\n"
                + "Papúa Nueva Guinea\n"
                + "Paraguay\n"
                + "Perú\n"
                + "Polonia\n"
                + "Portugal\n"
                + "Reino Unido\n"
                + "República Centroafricana\n"
                + "República Checa\n"
                + "República de Macedonia\n"
                + "República del Congo\n"
                + "República Democrática del Congo\n"
                + "República Dominicana\n"
                + "República Sudafricana\n"
                + "Ruanda\n"
                + "Rumanía\n"
                + "Rusia\n"
                + "Samoa\n"
                + "San Cristóbal y Nieves\n"
                + "San Marino\n"
                + "San Vicente y las Granadinas\n"
                + "Santa Lucía\n"
                + "Santo Tomé y Príncipe\n"
                + "Senegal\n"
                + "Serbia\n"
                + "Seychelles\n"
                + "Sierra Leona\n"
                + "Singapur\n"
                + "Siria\n"
                + "Somalia\n"
                + "Sri Lanka\n"
                + "Suazilandia\n"
                + "Sudán\n"
                + "Sudán del Sur\n"
                + "Suecia\n"
                + "Suiza\n"
                + "Surinam\n"
                + "Tailandia\n"
                + "Tanzania\n"
                + "Tayikistán\n"
                + "Timor Oriental\n"
                + "Togo\n"
                + "Tonga\n"
                + "Trinidad y Tobago\n"
                + "Túnez\n"
                + "Turkmenistán\n"
                + "Turquía\n"
                + "Tuvalu\n"
                + "Ucrania\n"
                + "Uganda\n"
                + "Uruguay\n"
                + "Uzbekistán\n"
                + "Vanuatu\n"
                + "Venezuela\n"
                + "Vietnam\n"
                + "Yemen\n"
                + "Yibuti\n"
                + "Zambia\n"
                + "Zimbabue";
        paises = paisesAux.split("\n");
    }

    private int indicePais(String pais) {
        int ret = -1;
        boolean ok = false;
        for (int i = 0; i < paises.length && !ok; i++) {
            if (pais.equals(paises[i])) {
                ret = i;
                ok = true;
            }
        }
        return ret;
    }

    public void cbSelectEvent() {
        cbPrincipal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                int index = cbPrincipal.getSelectedIndex();
                Node temp = (Node) tree.getListaPersonas().get(index);
                actualizarArbol(temp);
                mostrarArbolGrafico();
            }
        });
    }

    public void actualizar() {
        actualizarArbol(personaSeleccionada());
    }

    public void actualizarArbol(Node nodo) {
        llenarBotones(nodo);
    }

    public void cargarDatos(Arbol t) {
        Persona principal = new Persona("Manuel", "Estevez", 29, 7, 90, true, "Uruguay");
        Persona padrePrincipal = new Persona("Arturo", "Estevez", 5, 5, 60, true, "Argentina");
        Persona madrePrincipal = new Persona("Carla", "Lau", 3, 2, 65, false, "Argentina");
        Persona pareja = new Persona("Sofia", "Basso", 17, 3, 92, false, "Uruguay");
        Persona padrePareja = new Persona("Juan", "Basso", 13, 11, 61, true, "Uruguay");
        Persona madrePareja = new Persona("Romilda", "Juarez", 10, 9, 63, false, "Uruguay");
        Persona hijo1 = new Persona("Lucio", "Estevez", 20, 10, 6, true, "Uruguay");
        Persona hijo2 = new Persona("Maria", "Estevez", 15, 3, 8, false, "Peru");
        Node nPrinc = new Node(principal, t);
        Node nPareja = new Node(pareja, t);
        Node nPadrePrinc = new Node(padrePrincipal, t);
        Node nMadrePrinc = new Node(madrePrincipal, t);
        Node nPadrePareja = new Node(padrePareja, t);
        Node nMadrePareja = new Node(madrePareja, t);
        Node nHijo1 = new Node(hijo1, t);
        Node nHijo2 = new Node(hijo2, t);
        nPrinc.agregarPareja(nPareja);
        nPadrePrinc.agregarPareja(nMadrePrinc);
        nPadrePareja.agregarPareja(nMadrePareja);
        nPrinc.setearPadres(nPadrePrinc.obtenerUnionActual());
        nPareja.setearPadres(nPadrePareja.obtenerUnionActual());
        nPrinc.obtenerUnionActual().agregarHijo(nHijo1);
        nPrinc.obtenerUnionActual().agregarHijo(nHijo2);
        t.setRaiz(nPrinc);
        llenarBotones(nPrinc);
    }

    public void llenarBotones(Node principal) {
        llenarPrincipal(principal);
        llenarPadres(principal);
        llenarHnos(principal);
        Node pareja = principal.getParejaActual();
        llenarPareja(pareja);
        llenarPadresPareja(pareja);
        llenarHijos(principal);
        llenarHnosPareja(pareja);

    }

    private String nombreAMostrar(Node nodo) {
        return nodo.getMiembro().getNombre() + " " + nodo.getMiembro().getApellido();
    }

    public void llenarPrincipal(Node nodo) {
        nodoPrincipal.setText(nombreAMostrar(nodo));
    }

    public void llenarPadres(Node nodo) {
        Node padre = nodo.getPadre();
        Node madre = nodo.getMadre();
        if (madre != null && padre != null) {
            nodoPadre.setText(nombreAMostrar(padre));
            nodoMadre.setText(nombreAMostrar(madre));
        } else {
            nodoPadre.setText("Agregar Padre");
            nodoMadre.setText("Agregar Madre");
        }

    }

    public void llenarPareja(Node nodo) {
        if (nodo != null) {
            nodoPareja.setText(nombreAMostrar(nodo));
        } else {
            nodoPareja.setText("Agregar pareja");
        }
    }

    public void llenarPadresPareja(Node nodo) {
        if (nodo != null) {
            Node padrePareja = nodo.getPadre();
            Node madrePareja = nodo.getMadre();
            if (padrePareja != null && madrePareja != null) {
                nodoPadrePareja.setText(nombreAMostrar(padrePareja));
                nodoMadrePareja.setText(nombreAMostrar(madrePareja));
            } else {
                nodoPadrePareja.setText("Agregar Padre");
                nodoMadrePareja.setText("Agregar madre");
            }
        } else {
            nodoPadrePareja.setText("Agregar Padre");
            nodoMadrePareja.setText("Agregar madre");
        }

    }

    private void cargarCB() {
        for (int i = 0; i < tree.getListaPersonas().size(); i++) {
            Node nTemp = (Node) tree.getListaPersonas().get(i);
            Persona pTemp = (Persona) nTemp.getMiembro();
            cbPrincipal.addItem(pTemp.getNombre() + " " + pTemp.getApellido());
        }
    }

    private void setTxtHijos() {
        nodoHijo1.setText("Agregar hijo");
        nodoHijo2.setText("Agregar hijo");
        nodoHijo3.setText("Agregar hijo");
        nodoHijo4.setText("Agregar hijo");
    }

    private void setTxtHnos() {
        nodoHermano1.setText("Agregar hno.");
        nodoHermano2.setText("Agregar hno.");
        nodoHermano3.setText("Agregar hno.");
    }

    private void setTxtHnosPareja() {
        nodoHermanoPareja1.setText("Agregar hno.");
        nodoHermanoPareja2.setText("Agregar hno.");
        nodoHermanoPareja3.setText("Agregar hno.");
    }

    public void llenarHijos(Node nodo) {
        ArrayList<Node> hijos = nodo.obtenerHijos();
        int size = hijos.size();
        switch (size) {
            case 0:
                setTxtHijos();
                break;
            case 1:
                setTxtHijos();
                nodoHijo1.setText(nombreAMostrar(hijos.get(0)));
                break;
            case 2:
                setTxtHijos();
                nodoHijo1.setText(nombreAMostrar(hijos.get(0)));
                nodoHijo2.setText(nombreAMostrar(hijos.get(1)));
                break;
            case 3:
                setTxtHijos();
                nodoHijo1.setText(nombreAMostrar(hijos.get(0)));
                nodoHijo2.setText(nombreAMostrar(hijos.get(1)));
                nodoHijo3.setText(nombreAMostrar(hijos.get(2)));
                break;
            case 4:
                nodoHijo1.setText(nombreAMostrar(hijos.get(0)));
                nodoHijo2.setText(nombreAMostrar(hijos.get(1)));
                nodoHijo3.setText(nombreAMostrar(hijos.get(2)));
                nodoHijo4.setText(nombreAMostrar(hijos.get(3)));
                break;
            default:
                break;
        }
    }

    public void llenarHnos(Node nodo) {
        ArrayList<Node> listaHnos = nodo.obtenerHermanos();
        int size = listaHnos.size();
        switch (size) {
            case 0:
                setTxtHnos();
                break;
            case 1:
                setTxtHnos();
                nodoHermano1.setText(nombreAMostrar(listaHnos.get(0)));
                break;
            case 2:
                setTxtHnos();
                nodoHermano1.setText(nombreAMostrar(listaHnos.get(0)));
                nodoHermano2.setText(nombreAMostrar(listaHnos.get(1)));
                break;
            case 3:
                nodoHermano1.setText(nombreAMostrar(listaHnos.get(0)));
                nodoHermano2.setText(nombreAMostrar(listaHnos.get(1)));
                nodoHermano3.setText(nombreAMostrar(listaHnos.get(2)));
                break;
            default:
                break;
        }
    }

    public void llenarHnosPareja(Node nodo) {
        if (nodo == null) {
            return;
        }
        ArrayList<Node> listaHnos = nodo.obtenerHermanos();
        int size = listaHnos.size();
        switch (size) {
            case 0:
                setTxtHnosPareja();
                break;
            case 1:
                setTxtHnosPareja();
                nodoHermanoPareja1.setText(nombreAMostrar(listaHnos.get(0)));
                break;
            case 2:
                setTxtHnosPareja();
                nodoHermanoPareja1.setText(nombreAMostrar(listaHnos.get(0)));
                nodoHermanoPareja2.setText(nombreAMostrar(listaHnos.get(1)));
                break;
            case 3:
                nodoHermanoPareja1.setText(nombreAMostrar(listaHnos.get(0)));
                nodoHermanoPareja2.setText(nombreAMostrar(listaHnos.get(1)));
                nodoHermanoPareja3.setText(nombreAMostrar(listaHnos.get(2)));
                break;
            default:
                break;
        }
    }

    public Node personaSeleccionada() {
        int index = cbPrincipal.getSelectedIndex();
        Node ret = (Node) tree.getListaPersonas().get(index);
        return ret;
    }

    private String mostrarUniones(Node nodo) {
        String res = "";
        for (int i = 0; i < nodo.getUniones().size(); i++) {
            Union uAux = nodo.getUniones().get(i);
            if (uAux.isActual()) {
                res += uAux.getPareja(nodo).getMiembro().toString() + " - ACTUAL\n";
            } else {
                res += uAux.getPareja(nodo).getMiembro().toString() + "\n";
            }
        }
        return res;
    }

    private void llenarCamposFicha(boolean nuevo) {
        if (nuevo) {
            txtAreaUniones.setText("");
            textFieldNombre.setEditable(true);
            textFieldApellido.setEditable(true);
            textFieldNombre.setText("");
            textFieldApellido.setText("");
            textFieldApodo.setText("");
            textFieldLugarNac.setText("");
            cbNacionalidad.setSelectedIndex(0);
            ImageIcon img = new ImageIcon("src/img/icono.png");
            Icon icono = new ImageIcon(img.getImage().getScaledInstance(200, 200, Image.SCALE_SMOOTH));
            labelIcono.setIcon(icono);
            if (hijo || padre) {
                textFieldApellido.setEditable(false);
                textFieldApellido.setText(personaSeleccionada().getMiembro().getApellido());
            }
        } else {
            Node nodo = personaSeleccionada();
            textFieldNombre.setText(nodo.getMiembro().getNombre());
            textFieldNombre.setEditable(false);
            textFieldApellido.setEditable(false);
            textFieldApellido.setText(nodo.getMiembro().getApellido());
            textFieldApodo.setText(nodo.getMiembro().getApodo());
            labelIcono.setIcon(new ImageIcon(nodo.getMiembro().getFotoPerfil().getImage().getScaledInstance(200, 200, Image.SCALE_SMOOTH)));
            txtAreaUniones.setText(mostrarUniones(nodo));
            cbNacionalidad.setSelectedIndex(indicePais(nodo.getMiembro().getNacionalidad()));
            if (nodo.getMiembro().isHombre()) {
                cbSexo.setSelectedIndex(0);
            } else {
                cbSexo.setSelectedIndex(1);
            }
            dcFechaNac.setDate(nodo.getMiembro().getFecha());
            textFieldLugarNac.setText(nodo.getMiembro().getLugarNac());
        }
    }

    public void actualizarFicha() {
        boolean nuevo = false;
        labelFechaC.setVisible(false);
        labelFechaD.setVisible(false);
        cbSexo.setEnabled(true);
        textFieldApellido.setEditable(true);
        dcFechaCas.hide();
        dcFechaDiv.hide();
        if (hijo) {
            tituloFP.setText("Agregar Hijo");
            textFieldApellido.setEditable(false);
            nuevo = true;
        } else if (union) {
            dcFechaCas.show();
            dcFechaDiv.show();
            tituloFP.setText("Agregar Union");
            nuevo = true;
            labelFechaC.setVisible(true);
            labelFechaD.setVisible(true);
        } else if (padre) {
            tituloFP.setText("Agregar Padre");
            nuevo = true;
            cbSexo.setSelectedIndex(0);
            textFieldApellido.setEditable(false);
            cbSexo.setEnabled(false);
        } else if (madre) {
            tituloFP.setText("Agregar Madre");
            nuevo = true;
            cbSexo.setSelectedIndex(1);
            cbSexo.setEnabled(false);
        } else {
            tituloFP.setText("Ficha Personal");
        }
        llenarCamposFicha(nuevo);
    }

    public void mostrarImagen() {

        Node nodo = personaSeleccionada();
        Persona p = nodo.getMiembro();
        ImageIcon icon = p.getListaImagenes().get(indxImg);
        int imgWidth = icon.getIconWidth();
        int imgHeigh = icon.getIconHeight();
        Dimension imgSize = new Dimension(imgWidth, imgHeigh);
        Dimension borde = new Dimension(420, 420);
        Dimension nueva = getScaledDimension(imgSize, borde);
        Image imgFinal = icon.getImage().getScaledInstance(nueva.width, nueva.height, Image.SCALE_SMOOTH);
        labelImg.setIcon(new ImageIcon(imgFinal));
    }

    //Funcion de stackOverFlow
    public static Dimension getScaledDimension(Dimension imgSize, Dimension boundary) {

        int original_width = imgSize.width;
        int original_height = imgSize.height;
        int bound_width = boundary.width;
        int bound_height = boundary.height;
        int new_width = original_width;
        int new_height = original_height;

        // first check if we need to scale width
        if (original_width > bound_width) {
            //scale width to fit
            new_width = bound_width;
            //scale height to maintain aspect ratio
            new_height = (new_width * original_height) / original_width;
        }

        // then check if we need to scale even with the new height
        if (new_height > bound_height) {
            //scale height to fit instead
            new_height = bound_height;
            //scale width to maintain aspect ratio
            new_width = (new_height * original_width) / original_height;
        }

        return new Dimension(new_width, new_height);
    }

    public void mostrarDoc() {
        Persona p = personaSeleccionada().getMiembro();
        ImageIcon icon = p.getListaDocs().get(indxDoc);
        Image img = icon.getImage().getScaledInstance(labelImg.getWidth(), labelImg.getHeight(), Image.SCALE_SMOOTH);
        labelImg.setIcon(new ImageIcon(img));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelFijo = new javax.swing.JPanel();
        btnArbGrafico = new javax.swing.JButton();
        btnArbTextual = new javax.swing.JButton();
        panelMenuGrafico = new javax.swing.JPanel();
        btnFichaPersonal = new javax.swing.JButton();
        btnAgregarHijo = new javax.swing.JButton();
        btnAgregarUnion = new javax.swing.JButton();
        btnAgregarPadre = new javax.swing.JButton();
        btnAgregarDoc = new javax.swing.JButton();
        btnAgregarImg = new javax.swing.JButton();
        cbPrincipal = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        btnAgregarMadre = new javax.swing.JButton();
        treeFam = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        panelPrincipal = new javax.swing.JPanel();
        panelArbTextual = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtAreaArbGrafico = new javax.swing.JTextArea();
        labelArbTxt = new javax.swing.JLabel();
        panelFichaPersonal = new javax.swing.JPanel();
        labelNombreFicha = new javax.swing.JLabel();
        labelApellidoFicha = new javax.swing.JLabel();
        labelApodoFicha = new javax.swing.JLabel();
        labelFechaNacFicha = new javax.swing.JLabel();
        labelLugarNacFicha = new javax.swing.JLabel();
        labelNacionalidadFicha = new javax.swing.JLabel();
        textFieldNombre = new javax.swing.JTextField();
        textFieldApodo = new javax.swing.JTextField();
        textFieldApellido = new javax.swing.JTextField();
        textFieldLugarNac = new javax.swing.JTextField();
        cbNacionalidad = new javax.swing.JComboBox<>();
        botonImagenesAFicha = new javax.swing.JButton();
        botonDocumentosAFicha = new javax.swing.JButton();
        tituloFP = new javax.swing.JLabel();
        labelIcono = new javax.swing.JLabel();
        botonAgregarIcono = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnSalirFicha = new javax.swing.JButton();
        cbSexo = new javax.swing.JComboBox<>();
        labelSexoFicha = new javax.swing.JLabel();
        labelFechaC = new javax.swing.JLabel();
        labelFechaD = new javax.swing.JLabel();
        dcFechaNac = new com.toedter.calendar.JDateChooser();
        dcFechaCas = new com.toedter.calendar.JDateChooser();
        dcFechaDiv = new com.toedter.calendar.JDateChooser();
        labelUniones = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtAreaUniones = new javax.swing.JTextArea();
        panelArbol = new javax.swing.JPanel();
        nodoPrincipal = new javax.swing.JButton();
        nodoPareja = new javax.swing.JButton();
        nodoPadre = new javax.swing.JButton();
        nodoMadre = new javax.swing.JButton();
        nodoPadrePareja = new javax.swing.JButton();
        nodoMadrePareja = new javax.swing.JButton();
        nodoHijo1 = new javax.swing.JButton();
        nodoHijo2 = new javax.swing.JButton();
        nodoHijo4 = new javax.swing.JButton();
        nodoHijo3 = new javax.swing.JButton();
        nodoHermano1 = new javax.swing.JButton();
        nodoHermano3 = new javax.swing.JButton();
        nodoHermano2 = new javax.swing.JButton();
        nodoHermanoPareja1 = new javax.swing.JButton();
        nodoHermanoPareja2 = new javax.swing.JButton();
        nodoHermanoPareja3 = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        labelRef = new javax.swing.JLabel();
        panelImagenes = new javax.swing.JPanel();
        btnAnterior = new javax.swing.JButton();
        btnSiguiente = new javax.swing.JButton();
        labelImg = new javax.swing.JLabel();
        tituloImg = new javax.swing.JLabel();
        btnVolver = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelFijo.setBackground(new java.awt.Color(0, 102, 102));
        panelFijo.setPreferredSize(new java.awt.Dimension(275, 600));

        btnArbGrafico.setText("Arbol Grafico");
        btnArbGrafico.setPreferredSize(new java.awt.Dimension(110, 30));
        btnArbGrafico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnArbGraficoActionPerformed(evt);
            }
        });

        btnArbTextual.setText("Arbol Textual");
        btnArbTextual.setPreferredSize(new java.awt.Dimension(110, 30));
        btnArbTextual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnArbTextualActionPerformed(evt);
            }
        });

        panelMenuGrafico.setBackground(new java.awt.Color(0, 153, 153));
        panelMenuGrafico.setPreferredSize(new java.awt.Dimension(250, 300));

        btnFichaPersonal.setText("Ver Ficha Personal");
        btnFichaPersonal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFichaPersonalActionPerformed(evt);
            }
        });

        btnAgregarHijo.setText("Agregar Hijo");
        btnAgregarHijo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarHijoActionPerformed(evt);
            }
        });

        btnAgregarUnion.setText("Agregar Union");
        btnAgregarUnion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarUnionActionPerformed(evt);
            }
        });

        btnAgregarPadre.setText("Agregar Padre");
        btnAgregarPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarPadreActionPerformed(evt);
            }
        });

        btnAgregarDoc.setText("Agregar Documentos");
        btnAgregarDoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarDocActionPerformed(evt);
            }
        });

        btnAgregarImg.setText("Agregar Imagenes");
        btnAgregarImg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarImgActionPerformed(evt);
            }
        });

        cbPrincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbPrincipalActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Ver Arbol de: ");

        btnAgregarMadre.setText("Agregar Madre");
        btnAgregarMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarMadreActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMenuGraficoLayout = new javax.swing.GroupLayout(panelMenuGrafico);
        panelMenuGrafico.setLayout(panelMenuGraficoLayout);
        panelMenuGraficoLayout.setHorizontalGroup(
            panelMenuGraficoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuGraficoLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(panelMenuGraficoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cbPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarDoc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAgregarImg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAgregarPadre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAgregarUnion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAgregarHijo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnFichaPersonal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAgregarMadre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(70, Short.MAX_VALUE))
        );
        panelMenuGraficoLayout.setVerticalGroup(
            panelMenuGraficoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuGraficoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFichaPersonal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAgregarHijo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAgregarUnion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAgregarPadre)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAgregarMadre)
                .addGap(11, 11, 11)
                .addComponent(btnAgregarImg)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAgregarDoc)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        treeFam.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        treeFam.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/LogoTransC.png"))); // NOI18N

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelFijoLayout = new javax.swing.GroupLayout(panelFijo);
        panelFijo.setLayout(panelFijoLayout);
        panelFijoLayout.setHorizontalGroup(
            panelFijoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFijoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(panelFijoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelFijoLayout.createSequentialGroup()
                        .addComponent(treeFam, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34))
                    .addGroup(panelFijoLayout.createSequentialGroup()
                        .addComponent(btnArbGrafico, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(btnArbTextual, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25))))
            .addGroup(panelFijoLayout.createSequentialGroup()
                .addGroup(panelFijoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelFijoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panelMenuGrafico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelFijoLayout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(13, Short.MAX_VALUE))
        );
        panelFijoLayout.setVerticalGroup(
            panelFijoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFijoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(treeFam)
                .addGap(28, 28, 28)
                .addGroup(panelFijoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnArbGrafico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnArbTextual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnSalir)
                .addGap(18, 18, 18)
                .addComponent(panelMenuGrafico, javax.swing.GroupLayout.DEFAULT_SIZE, 312, Short.MAX_VALUE)
                .addGap(70, 70, 70))
        );

        panelPrincipal.setBackground(new java.awt.Color(51, 255, 255));
        panelPrincipal.setLayout(new java.awt.CardLayout());

        panelArbTextual.setBackground(new java.awt.Color(0, 153, 153));

        txtAreaArbGrafico.setColumns(20);
        txtAreaArbGrafico.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        txtAreaArbGrafico.setRows(5);
        txtAreaArbGrafico.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jScrollPane1.setViewportView(txtAreaArbGrafico);

        labelArbTxt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        labelArbTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelArbTxt.setText("Arbol Textual");

        javax.swing.GroupLayout panelArbTextualLayout = new javax.swing.GroupLayout(panelArbTextual);
        panelArbTextual.setLayout(panelArbTextualLayout);
        panelArbTextualLayout.setHorizontalGroup(
            panelArbTextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelArbTextualLayout.createSequentialGroup()
                .addGap(328, 328, 328)
                .addGroup(panelArbTextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 663, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelArbTextualLayout.createSequentialGroup()
                        .addComponent(labelArbTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(76, 76, 76)))
                .addContainerGap(222, Short.MAX_VALUE))
        );
        panelArbTextualLayout.setVerticalGroup(
            panelArbTextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelArbTextualLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(labelArbTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 433, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(93, Short.MAX_VALUE))
        );

        panelPrincipal.add(panelArbTextual, "card3");

        panelFichaPersonal.setBackground(new java.awt.Color(0, 153, 153));
        panelFichaPersonal.setToolTipText("Ficha Personal");

        labelNombreFicha.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelNombreFicha.setForeground(new java.awt.Color(255, 255, 255));
        labelNombreFicha.setText("Nombre");

        labelApellidoFicha.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelApellidoFicha.setForeground(new java.awt.Color(255, 255, 255));
        labelApellidoFicha.setText("Apellido");

        labelApodoFicha.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelApodoFicha.setForeground(new java.awt.Color(255, 255, 255));
        labelApodoFicha.setText("Apodo");

        labelFechaNacFicha.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelFechaNacFicha.setForeground(new java.awt.Color(255, 255, 255));
        labelFechaNacFicha.setText("Fecha Nac.");

        labelLugarNacFicha.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelLugarNacFicha.setForeground(new java.awt.Color(255, 255, 255));
        labelLugarNacFicha.setText("Lugar de Nacimiento");

        labelNacionalidadFicha.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelNacionalidadFicha.setForeground(new java.awt.Color(255, 255, 255));
        labelNacionalidadFicha.setText("Nacionalidad");

        textFieldNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldNombreActionPerformed(evt);
            }
        });

        cbNacionalidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Afganistán", "Albania", "Alemania", "Andorra", "Angola", "Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria", "Azerbaiyán", "Bahamas", "Bangladés", "Barbados", "Baréin", "Bélgica", "Belice", "Benín", "Bielorrusia", "Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brunéi", "Bulgaria", "Burkina Faso", "Burundi", "Bután", "Cabo Verde", "Camboya", "Camerún", "Canadá", "Catar", "Chad", "Chile", "China", "Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur", "Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto", "El Salvador", "Emiratos Árabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "España", "Estados Unidos", "Estonia", "Etiopía", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gabón", "Gambia", "Georgia", "Ghana", "Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bisáu", "Haití", "Honduras", "Hungría", "India", "Indonesia", "Irak", "Irán", "Irlanda", "Islandia", "Islas Marshall", "Islas Salomón", "Israel", "Italia", "Jamaica", "Japón", "Jordania", "Kazajistán", "Kenia", "Kirguistán", "Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "Líbano", "Liberia", "Libia", "Liechtenstein", "Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Malí", "Malta", "Marruecos", "Mauricio", "Mauritania", "México", "Micronesia", "Moldavia", "Mónaco", "Mongolia", "Montenegro", "Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "Níger", "Nigeria", "Noruega", "Nueva Zelanda", "Omán", "Países Bajos", "Pakistán", "Palaos", "Panamá", "Papúa Nueva Guinea", "Paraguay", "Perú", "Polonia", "Portugal", "Reino Unido", "República Centroafricana", "República Checa", "República de Macedonia", "República del Congo", "República Democrática del Congo", "República Dominicana", "República Sudafricana", "Ruanda", "Rumanía", "Rusia", "Samoa", "San Cristóbal y Nieves", "San Marino", "San Vicente y las Granadinas", "Santa Lucía", "Santo Tomé y Príncipe", "Senegal", "Serbia", "Seychelles", "Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sudán", "Sudán del Sur", "Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikistán", "Timor Oriental", "Togo", "Tonga", "Trinidad y Tobago", "Túnez", "Turkmenistán", "Turquía", "Tuvalu", "Ucrania", "Uganda", "Uruguay", "Uzbekistán", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue" }));

        botonImagenesAFicha.setText("Ver imagenes");
        botonImagenesAFicha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonImagenesAFichaActionPerformed(evt);
            }
        });

        botonDocumentosAFicha.setText("Ver Documentos");
        botonDocumentosAFicha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDocumentosAFichaActionPerformed(evt);
            }
        });

        tituloFP.setFont(new java.awt.Font("Times New Roman", 0, 48)); // NOI18N
        tituloFP.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tituloFP.setText("Ficha Personal");

        labelIcono.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        botonAgregarIcono.setText("Seleccionar icono personal");
        botonAgregarIcono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarIconoActionPerformed(evt);
            }
        });

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnSalirFicha.setText("Salir");
        btnSalirFicha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirFichaActionPerformed(evt);
            }
        });

        cbSexo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Hombre", "Mujer" }));
        cbSexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbSexoActionPerformed(evt);
            }
        });

        labelSexoFicha.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelSexoFicha.setForeground(new java.awt.Color(255, 255, 255));
        labelSexoFicha.setText("Sexo");

        labelFechaC.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelFechaC.setForeground(new java.awt.Color(255, 255, 255));
        labelFechaC.setText("Fecha de Casados");

        labelFechaD.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelFechaD.setForeground(new java.awt.Color(255, 255, 255));
        labelFechaD.setText("Fecha de Divorcio");

        dcFechaNac.setDateFormatString("dd/MM/yyyy");

        dcFechaCas.setDateFormatString("dd/MM/yyyy");

        dcFechaDiv.setDateFormatString("dd/MM/yyyy");

        labelUniones.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelUniones.setForeground(new java.awt.Color(255, 255, 255));
        labelUniones.setText("Uniones");

        txtAreaUniones.setColumns(20);
        txtAreaUniones.setRows(5);
        jScrollPane3.setViewportView(txtAreaUniones);

        javax.swing.GroupLayout panelFichaPersonalLayout = new javax.swing.GroupLayout(panelFichaPersonal);
        panelFichaPersonal.setLayout(panelFichaPersonalLayout);
        panelFichaPersonalLayout.setHorizontalGroup(
            panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                .addGap(262, 262, 262)
                .addComponent(tituloFP, javax.swing.GroupLayout.PREFERRED_SIZE, 458, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                        .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                                            .addComponent(labelLugarNacFicha)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFichaPersonalLayout.createSequentialGroup()
                                            .addComponent(labelNacionalidadFicha)
                                            .addGap(57, 57, 57)))
                                    .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                                        .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(labelSexoFicha, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(labelApellidoFicha, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(labelNombreFicha, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(46, 46, 46)))
                                .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                                    .addComponent(labelFechaC, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(24, 24, 24)))
                            .addComponent(labelUniones, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                                .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                                        .addComponent(dcFechaCas, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(labelFechaD, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(cbNacionalidad, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(textFieldLugarNac)
                                        .addComponent(textFieldApellido)
                                        .addComponent(textFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(cbSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(dcFechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                                            .addComponent(botonImagenesAFicha, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(botonDocumentosAFicha))))
                                .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                                        .addGap(0, 177, Short.MAX_VALUE)
                                        .addComponent(labelApodoFicha)
                                        .addGap(18, 18, 18))
                                    .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(dcFechaDiv, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textFieldApodo, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelFichaPersonalLayout.createSequentialGroup()
                                    .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnSalirFicha, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(botonAgregarIcono, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelIcono, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE))))
                    .addComponent(labelFechaNacFicha))
                .addGap(311, 311, 311))
        );
        panelFichaPersonalLayout.setVerticalGroup(
            panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tituloFP, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNombreFicha, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelApodoFicha)
                    .addComponent(textFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldApodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(labelIcono, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonAgregarIcono, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(43, 43, 43)
                        .addComponent(labelUniones, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelFichaPersonalLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelApellidoFicha)
                            .addComponent(textFieldApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelFechaNacFicha)
                            .addComponent(dcFechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelLugarNacFicha)
                            .addComponent(textFieldLugarNac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelFechaD)
                            .addComponent(labelFechaC)
                            .addComponent(dcFechaCas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dcFechaDiv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelSexoFicha)
                            .addComponent(cbSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(35, 35, 35)
                        .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelNacionalidadFicha)
                            .addComponent(cbNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addGroup(panelFichaPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonImagenesAFicha, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonDocumentosAFicha, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalirFicha, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38))
        );

        panelPrincipal.add(panelFichaPersonal, "card5");

        panelArbol.setBackground(new java.awt.Color(0, 153, 153));
        panelArbol.setPreferredSize(new java.awt.Dimension(1000, 600));

        nodoPrincipal.setBackground(new java.awt.Color(255, 102, 102));
        nodoPrincipal.setText("Principal");
        nodoPrincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nodoPrincipalActionPerformed(evt);
            }
        });

        nodoPareja.setBackground(new java.awt.Color(255, 153, 51));
        nodoPareja.setText("Pareja");

        nodoPadre.setBackground(new java.awt.Color(255, 102, 255));
        nodoPadre.setText("Padre");
        nodoPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nodoPadreActionPerformed(evt);
            }
        });

        nodoMadre.setBackground(new java.awt.Color(204, 0, 102));
        nodoMadre.setText("Madre");

        nodoPadrePareja.setBackground(new java.awt.Color(153, 255, 102));
        nodoPadrePareja.setText("Padre Pareja");

        nodoMadrePareja.setBackground(new java.awt.Color(51, 204, 0));
        nodoMadrePareja.setText("Madre Pareja");

        nodoHijo1.setBackground(new java.awt.Color(153, 204, 0));
        nodoHijo1.setText("Hijo1");
        nodoHijo1.setMaximumSize(new java.awt.Dimension(70, 30));
        nodoHijo1.setMinimumSize(new java.awt.Dimension(70, 30));
        nodoHijo1.setPreferredSize(new java.awt.Dimension(70, 30));

        nodoHijo2.setBackground(new java.awt.Color(153, 204, 0));
        nodoHijo2.setText("Hijo2");

        nodoHijo4.setBackground(new java.awt.Color(153, 204, 0));
        nodoHijo4.setText("Hijo4");

        nodoHijo3.setBackground(new java.awt.Color(153, 204, 0));
        nodoHijo3.setText("Hijo 3");

        nodoHermano1.setBackground(new java.awt.Color(255, 102, 102));
        nodoHermano1.setText("Hermano1");

        nodoHermano3.setBackground(new java.awt.Color(255, 102, 102));
        nodoHermano3.setText("Hermano3");

        nodoHermano2.setBackground(new java.awt.Color(255, 102, 102));
        nodoHermano2.setText("Hermano2");

        nodoHermanoPareja1.setBackground(new java.awt.Color(255, 153, 51));
        nodoHermanoPareja1.setText("Hermano1");

        nodoHermanoPareja2.setBackground(new java.awt.Color(255, 153, 51));
        nodoHermanoPareja2.setText("Hermano2");

        nodoHermanoPareja3.setBackground(new java.awt.Color(255, 153, 51));
        nodoHermanoPareja3.setText("Hermano3");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 204, 51));
        jLabel1.setText("_________");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 204, 51));
        jLabel3.setText("_______");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 204, 51));
        jLabel4.setText("_____");

        labelRef.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        labelRef.setPreferredSize(new java.awt.Dimension(200, 200));

        javax.swing.GroupLayout panelArbolLayout = new javax.swing.GroupLayout(panelArbol);
        panelArbol.setLayout(panelArbolLayout);
        panelArbolLayout.setHorizontalGroup(
            panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelArbolLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(panelArbolLayout.createSequentialGroup()
                        .addComponent(nodoHermano1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(nodoHermano2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(nodoHermano3, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(nodoPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelArbolLayout.createSequentialGroup()
                        .addComponent(nodoHijo1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(nodoHijo2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelArbolLayout.createSequentialGroup()
                        .addComponent(nodoHijo3, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(nodoHijo4, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(labelRef, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelArbolLayout.createSequentialGroup()
                        .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelArbolLayout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nodoPareja, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nodoHermanoPareja1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelArbolLayout.createSequentialGroup()
                                .addComponent(nodoPadrePareja, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nodoHermanoPareja2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nodoMadrePareja, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nodoHermanoPareja3, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelArbolLayout.createSequentialGroup()
                .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelArbolLayout.createSequentialGroup()
                        .addGap(153, 153, 153)
                        .addComponent(nodoPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nodoMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2))
                    .addGroup(panelArbolLayout.createSequentialGroup()
                        .addGap(498, 498, 498)
                        .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelArbolLayout.setVerticalGroup(
            panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelArbolLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(nodoPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)
                        .addComponent(jLabel1)
                        .addComponent(nodoMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(nodoPadrePareja, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(nodoMadrePareja, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)))
                .addGap(19, 19, 19)
                .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nodoHermano1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nodoHermano2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nodoHermano3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(nodoPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4))
                    .addComponent(nodoPareja, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nodoHermanoPareja1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(nodoHermanoPareja3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(nodoHermanoPareja2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelArbolLayout.createSequentialGroup()
                        .addGap(186, 186, 186)
                        .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(nodoHijo1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(nodoHijo2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelArbolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(nodoHijo3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(nodoHijo4, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(34, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelArbolLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(labelRef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))))
        );

        panelPrincipal.add(panelArbol, "card4");

        panelImagenes.setBackground(new java.awt.Color(0, 153, 153));
        panelImagenes.setPreferredSize(new java.awt.Dimension(1000, 600));

        btnAnterior.setText("Anterior");
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });

        btnSiguiente.setText("Siguiente");
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        labelImg.setPreferredSize(new java.awt.Dimension(420, 420));

        tituloImg.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tituloImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tituloImg.setText("Imágenes");

        btnVolver.setText("Volver");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelImagenesLayout = new javax.swing.GroupLayout(panelImagenes);
        panelImagenes.setLayout(panelImagenesLayout);
        panelImagenesLayout.setHorizontalGroup(
            panelImagenesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImagenesLayout.createSequentialGroup()
                .addContainerGap(360, Short.MAX_VALUE)
                .addComponent(btnAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(124, 124, 124)
                .addComponent(btnVolver, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(114, 114, 114))
            .addGroup(panelImagenesLayout.createSequentialGroup()
                .addGap(279, 279, 279)
                .addComponent(labelImg, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(panelImagenesLayout.createSequentialGroup()
                .addGap(233, 233, 233)
                .addComponent(tituloImg, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelImagenesLayout.setVerticalGroup(
            panelImagenesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImagenesLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(tituloImg, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelImg, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(panelImagenesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVolver, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(64, Short.MAX_VALUE))
        );

        panelPrincipal.add(panelImagenes, "card5");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelFijo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelFijo, javax.swing.GroupLayout.DEFAULT_SIZE, 634, Short.MAX_VALUE)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnArbGraficoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnArbGraficoActionPerformed
        mostrarArbolGrafico();
    }//GEN-LAST:event_btnArbGraficoActionPerformed

    private void btnArbTextualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnArbTextualActionPerformed
        panelPrincipal.removeAll();
        panelMenuGrafico.hide();
        panelPrincipal.add(panelArbTextual);
        panelPrincipal.repaint();
        panelPrincipal.revalidate();
        txtAreaArbGrafico.setText(tree.ordenarArbolAlf());
    }//GEN-LAST:event_btnArbTextualActionPerformed


    private void cbPrincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbPrincipalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbPrincipalActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void textFieldNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldNombreActionPerformed

    private void botonDocumentosAFichaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDocumentosAFichaActionPerformed
        img = false;
        doc = true;
        indxDoc = 0;
        tituloImg.setText("Documentos");
        if (personaSeleccionada().getMiembro().getListaDocs().size() == 0) {
            JOptionPane.showMessageDialog(null, "No tiene documentos cargados", "Error", 0);
        } else {
            mostrarPanelDoc();
        }
    }//GEN-LAST:event_botonDocumentosAFichaActionPerformed

    private void botonAgregarIconoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarIconoActionPerformed

        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "png", "jpeg", "gif");
        fc.setFileFilter(filter);
        int opcion = fc.showOpenDialog(null);
        if (opcion == JFileChooser.APPROVE_OPTION) {
            String file = fc.getSelectedFile().getPath();
            ImageIcon img = new ImageIcon(file);
            Icon icono = new ImageIcon(img.getImage().getScaledInstance(200, 200, Image.SCALE_SMOOTH));
            labelIcono.setIcon(icono);
            personaSeleccionada().getMiembro().setFotoPerfil(img);
        }
    }//GEN-LAST:event_botonAgregarIconoActionPerformed

    private void mostrarFichaPersonal() {
        actualizarFicha();
        panelPrincipal.removeAll();
        panelMenuGrafico.show();
        panelPrincipal.add(panelFichaPersonal);
        panelPrincipal.repaint();
        panelPrincipal.revalidate();
    }
    private void btnFichaPersonalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFichaPersonalActionPerformed
        hijo = false;
        union = false;
        padre = false;
        madre = false;
        mostrarFichaPersonal();
    }//GEN-LAST:event_btnFichaPersonalActionPerformed

    private void cbSexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbSexoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbSexoActionPerformed

    private void mostrarArbolGrafico() {
        panelPrincipal.removeAll();
        panelMenuGrafico.show();
        panelPrincipal.add(panelArbol);
        panelPrincipal.repaint();
        panelPrincipal.revalidate();
    }
    private void btnSalirFichaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirFichaActionPerformed
        mostrarArbolGrafico();
    }//GEN-LAST:event_btnSalirFichaActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        Node nodo = personaSeleccionada();
        String nombre = textFieldNombre.getText();
        String apodo = textFieldApodo.getText();
        String apellido = textFieldApellido.getText();
        int index = cbSexo.getSelectedIndex();
        String nacionalidad = (String) cbNacionalidad.getSelectedItem();
        String lugarNac = textFieldLugarNac.getText();
        Date fecha = dcFechaNac.getDate();
        if (hijo || union || padre || madre) {
            Persona persona = new Persona();
            persona.setNombre(nombre);
            persona.setApellido(apellido);
            persona.setApodo(apodo);
            persona.setNacionalidad(nacionalidad);
            persona.setLugarNac(lugarNac);
            persona.setFecha(fecha);
            if (index == 0) {
                persona.setEsHombre(true);
            } else {
                persona.setEsHombre(false);
            }
            Node nodoNuevo = new Node(persona, tree);
            if (hijo) {
                Union matrimonio = nodo.obtenerUnionActual();
                matrimonio.agregarHijo(nodoNuevo);
            } else if (madre) {
                nodo.setMadre(nodoNuevo);
                Node nPadre = nodo.getPadre();
                if (nPadre != null) {
                    nPadre.agregarPareja(nodoNuevo);
                    nodo.setearPadres(nPadre.obtenerUnionActual());
                }
            } else if (padre) {
                nodo.setPadre(nodoNuevo);
                Node nMadre = nodo.getMadre();
                if (nMadre != null) {
                    nMadre.agregarPareja(nodoNuevo);
                    nodo.setearPadres(nMadre.obtenerUnionActual());
                }
            } else {
                Date fechaCasados = dcFechaCas.getDate();
                Date fechaDiv = dcFechaDiv.getDate();
                nodo.agregarPareja(nodoNuevo);
                Union matrimonio = nodo.obtenerUnionActual();
                matrimonio.setFechaInicio(fechaCasados);
                matrimonio.setFechaFin(fechaDiv);
            }
            cbPrincipal.addItem(nombreAMostrar(nodoNuevo));
        } else {
            Persona miembro = nodo.getMiembro();
            miembro.setNombre(nombre);
            miembro.setApellido(apellido);
            miembro.setApodo(apodo);
            miembro.setNacionalidad(nacionalidad);
            miembro.setLugarNac(lugarNac);
            if (index == 0) {
                miembro.setEsHombre(true);
            } else {
                miembro.setEsHombre(false);
            }

        }
        actualizar();
        mostrarArbolGrafico();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnAgregarImgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarImgActionPerformed
        Node nodo = personaSeleccionada();
        if (nodo == null) {
            JOptionPane.showMessageDialog(this, "Error");
        } else {
            JFileChooser fc = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "png", "jpeg", "gif");
            fc.setFileFilter(filter);
            int opcion = fc.showOpenDialog(null);
            if (opcion == JFileChooser.APPROVE_OPTION) {
                String file = fc.getSelectedFile().getPath();
                ImageIcon img = new ImageIcon(file);
                nodo.getMiembro().getListaImagenes().add(img);
                JOptionPane.showMessageDialog(this, "Imagenes cargadas exitosamente", "Exito", 1);
            }
        }


    }//GEN-LAST:event_btnAgregarImgActionPerformed

    private void btnAgregarDocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarDocActionPerformed
        Node nodo = personaSeleccionada();
        if (nodo == null) {
            JOptionPane.showMessageDialog(this, "Error");
        } else {
            JFileChooser fc = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "png", "jpeg", "gif");
            fc.setFileFilter(filter);
            int opcion = fc.showOpenDialog(null);
            if (opcion == JFileChooser.APPROVE_OPTION) {
                String file = fc.getSelectedFile().getPath();
                ImageIcon img = new ImageIcon(file);
                nodo.getMiembro().getListaDocs().add(img);
                JOptionPane.showMessageDialog(this, "Documentos cargados exitosamente", "Exito", 1);
            }
        }

    }//GEN-LAST:event_btnAgregarDocActionPerformed

    private void btnAgregarHijoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarHijoActionPerformed
        union = false;
        padre = false;
        madre = false;
        Node principal = personaSeleccionada();
        if (principal.getParejaActual() == null) {
            JOptionPane.showMessageDialog(this, "Debe tener pareja actual", "Error", 0);
        } else if (principal.obtenerUnionActual().getListaHijos().size() == 4) {
            JOptionPane.showMessageDialog(this, "Ya tiene 4 hijos", "Error", 0);
        } else {
            hijo = true;
            mostrarFichaPersonal();
            Node pareja = principal.getParejaActual();
        }
    }//GEN-LAST:event_btnAgregarHijoActionPerformed

    private void btnAgregarUnionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarUnionActionPerformed
        union = true;
        padre = false;
        hijo = false;
        madre = false;
        mostrarFichaPersonal();
    }//GEN-LAST:event_btnAgregarUnionActionPerformed

    private void btnAgregarPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarPadreActionPerformed
        Node nodo = personaSeleccionada();
        if (nodo.getPadre() != null) {
            JOptionPane.showMessageDialog(this, "Ya tiene padre", "Error", 0);
        } else {
            union = false;
            padre = true;
            hijo = false;
            madre = false;
            mostrarFichaPersonal();
        }
    }//GEN-LAST:event_btnAgregarPadreActionPerformed

    private void btnAgregarMadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarMadreActionPerformed
        Node nodo = personaSeleccionada();
        if (nodo.getMadre() != null) {
            JOptionPane.showMessageDialog(this, "Ya tiene madre", "Error", 0);
        } else {
            union = false;
            padre = false;
            hijo = false;
            madre = true;
            mostrarFichaPersonal();
        }
    }//GEN-LAST:event_btnAgregarMadreActionPerformed

    private void mostrarPanelImg() {
        mostrarImagen();
        panelPrincipal.removeAll();
        panelMenuGrafico.show();
        panelPrincipal.add(panelImagenes);
        panelPrincipal.repaint();
        panelPrincipal.revalidate();
    }

    private void mostrarPanelDoc() {
        mostrarDoc();
        panelPrincipal.removeAll();
        panelMenuGrafico.show();
        panelPrincipal.add(panelImagenes);
        panelPrincipal.repaint();
        panelPrincipal.revalidate();
    }
    private void botonImagenesAFichaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonImagenesAFichaActionPerformed
        doc = false;
        img = true;
        indxImg = 0;
        tituloImg.setText("Imágenes");
        Node nodo = personaSeleccionada();
        Persona p = nodo.getMiembro();
        if (p.getListaImagenes().size() == 0) {
            JOptionPane.showMessageDialog(this, "No tiene imagenes cargadas", "Error", 0);
        } else {
            mostrarPanelImg();
        }
    }//GEN-LAST:event_botonImagenesAFichaActionPerformed

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        Persona p = personaSeleccionada().getMiembro();
        if (img) {
            if (p.getListaImagenes().size() > indxImg + 1) {
                indxImg++;
            } else {
                indxImg = 0;
            }
            mostrarImagen();
        } else {
            if (p.getListaDocs().size() > indxDoc + 1) {
                indxDoc++;
            } else {
                indxDoc = 0;
            }
            mostrarDoc();
        }
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        if (img) {
            if (indxImg == 0) {
                indxImg = personaSeleccionada().getMiembro().getListaImagenes().size() - 1;
            } else {
                indxImg--;
            }
            mostrarImagen();
        } else {
            if (indxDoc == 0) {
                indxDoc = personaSeleccionada().getMiembro().getListaDocs().size() - 1;
            } else {
                indxDoc--;
            }
            mostrarDoc();
        }
    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void nodoPrincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nodoPrincipalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nodoPrincipalActionPerformed

    private void nodoPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nodoPadreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nodoPadreActionPerformed

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        mostrarFichaPersonal();
    }//GEN-LAST:event_btnVolverActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAgregarIcono;
    private javax.swing.JButton botonDocumentosAFicha;
    private javax.swing.JButton botonImagenesAFicha;
    private javax.swing.JButton btnAgregarDoc;
    private javax.swing.JButton btnAgregarHijo;
    private javax.swing.JButton btnAgregarImg;
    private javax.swing.JButton btnAgregarMadre;
    private javax.swing.JButton btnAgregarPadre;
    private javax.swing.JButton btnAgregarUnion;
    private javax.swing.JButton btnAnterior;
    private javax.swing.JButton btnArbGrafico;
    private javax.swing.JButton btnArbTextual;
    private javax.swing.JButton btnFichaPersonal;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnSalirFicha;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JButton btnVolver;
    private javax.swing.JComboBox<String> cbNacionalidad;
    private javax.swing.JComboBox<String> cbPrincipal;
    private javax.swing.JComboBox<String> cbSexo;
    private com.toedter.calendar.JDateChooser dcFechaCas;
    private com.toedter.calendar.JDateChooser dcFechaDiv;
    private com.toedter.calendar.JDateChooser dcFechaNac;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel labelApellidoFicha;
    private javax.swing.JLabel labelApodoFicha;
    private javax.swing.JLabel labelArbTxt;
    private javax.swing.JLabel labelFechaC;
    private javax.swing.JLabel labelFechaD;
    private javax.swing.JLabel labelFechaNacFicha;
    private javax.swing.JLabel labelIcono;
    private javax.swing.JLabel labelImg;
    private javax.swing.JLabel labelLugarNacFicha;
    private javax.swing.JLabel labelNacionalidadFicha;
    private javax.swing.JLabel labelNombreFicha;
    private javax.swing.JLabel labelRef;
    private javax.swing.JLabel labelSexoFicha;
    private javax.swing.JLabel labelUniones;
    private javax.swing.JButton nodoHermano1;
    private javax.swing.JButton nodoHermano2;
    private javax.swing.JButton nodoHermano3;
    private javax.swing.JButton nodoHermanoPareja1;
    private javax.swing.JButton nodoHermanoPareja2;
    private javax.swing.JButton nodoHermanoPareja3;
    private javax.swing.JButton nodoHijo1;
    private javax.swing.JButton nodoHijo2;
    private javax.swing.JButton nodoHijo3;
    private javax.swing.JButton nodoHijo4;
    private javax.swing.JButton nodoMadre;
    private javax.swing.JButton nodoMadrePareja;
    private javax.swing.JButton nodoPadre;
    private javax.swing.JButton nodoPadrePareja;
    private javax.swing.JButton nodoPareja;
    private javax.swing.JButton nodoPrincipal;
    private javax.swing.JPanel panelArbTextual;
    private javax.swing.JPanel panelArbol;
    private javax.swing.JPanel panelFichaPersonal;
    private javax.swing.JPanel panelFijo;
    private javax.swing.JPanel panelImagenes;
    private javax.swing.JPanel panelMenuGrafico;
    private javax.swing.JPanel panelPrincipal;
    private javax.swing.JTextField textFieldApellido;
    private javax.swing.JTextField textFieldApodo;
    private javax.swing.JTextField textFieldLugarNac;
    private javax.swing.JTextField textFieldNombre;
    private javax.swing.JLabel tituloFP;
    private javax.swing.JLabel tituloImg;
    private javax.swing.JLabel treeFam;
    private javax.swing.JTextArea txtAreaArbGrafico;
    private javax.swing.JTextArea txtAreaUniones;
    // End of variables declaration//GEN-END:variables
}
