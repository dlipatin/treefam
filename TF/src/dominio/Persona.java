package dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.ImageIcon;

public class Persona implements Comparable<Persona> {

    private String nombre;
    private String apellido;
    private String apodo;
    private boolean hombre;
    private Date fecha;
    private String nacionalidad;
    private String lugarNac;
    private ImageIcon fotoPerfil;
    private ArrayList<ImageIcon> listaImagenes;
    private ArrayList<ImageIcon> listaDocs;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public boolean isHombre() {
        return hombre;
    }

    public void setEsHombre(boolean esHombre) {
        this.hombre = esHombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getLugarNac() {
        return lugarNac;
    }

    public void setLugarNac(String lugarNac) {
        this.lugarNac = lugarNac;
    }

    public ImageIcon getFotoPerfil() {
        return fotoPerfil;
    }

    public void setFotoPerfil(ImageIcon fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }

    public ArrayList<ImageIcon> getListaImagenes() {
        return listaImagenes;
    }

    public void setListaImagenes(ArrayList<ImageIcon> listaImagenes) {
        this.listaImagenes = listaImagenes;
    }

    public ArrayList<ImageIcon> getListaDocs() {
        return listaDocs;
    }

    public void setListaDocs(ArrayList<ImageIcon> listaDocs) {
        this.listaDocs = listaDocs;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public Persona() {
        this.setNombre("No Name");
        this.setApellido("No Last Name");
        this.setApodo("No Surname");
        this.setNacionalidad("No Nation");
        this.setLugarNac("Hospital");
        this.setFecha(new Date());
        this.setEsHombre(true);
        this.setFotoPerfil(new ImageIcon("src/img/icono.png"));
        this.setListaDocs(new ArrayList<ImageIcon>());
        this.setListaImagenes(new ArrayList<ImageIcon>());
    }

    public Persona(String nombre, String apellido, int day, int month, int year, boolean esHombre, String nacionalidad) {
        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setEsHombre(esHombre);
        this.setFecha(new Date(year,month-1,day));
        this.setFotoPerfil(new ImageIcon("src/img/icono.png"));
        this.setApodo("No Surname");
        this.setNacionalidad(nacionalidad);
        this.setLugarNac("Hospital");
        this.setListaImagenes(new ArrayList<ImageIcon>());
        this.setListaDocs(new ArrayList<ImageIcon>());
    }

    @Override
    public String toString() {
        return apellido + " " + nombre;
    }

    
    public boolean equals(Persona p) {
        boolean ret = false;
        if (this.getNombre().equals(p.getNombre()) && this.getApellido().equals(p.getApellido()) && this.getApodo().equals((p).getApodo())) {
            ret = true;
        }
        return ret;
    }

    @Override
    public int compareTo(Persona p) {
        return (this.getApellido().compareTo(p.getApellido()));
        
    }
}
