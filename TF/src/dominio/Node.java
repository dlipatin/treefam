package dominio;

import java.util.ArrayList;

public class Node {

    private Arbol tree;
    private Node padre;
    private Node madre;
    private Persona miembro;
    private ArrayList<Union> uniones;

    public Node(Persona p, Arbol ar) {
        this.setTree(ar);
        this.setMiembro(p);
        this.setUniones(new ArrayList<Union>());
        this.setPadre(null);
        this.setMadre(null);
        this.getTree().getListaPersonas().add(this);
    }

    public Arbol getTree() {
        return tree;
    }

    public void setTree(Arbol tree) {
        this.tree = tree;
    }

    public Node getPadre() {
        return padre;
    }

    public void setPadre(Node padre) {
        this.padre = padre;
    }

    public Node getMadre() {
        return madre;
    }

    public void setMadre(Node madre) {
        this.madre = madre;
    }
  

    public Persona getMiembro() {
        return miembro;
    }

    public void setMiembro(Persona miembro) {
        this.miembro = miembro;
    }

    public ArrayList<Union> getUniones() {
        return uniones;
    }

    public void setUniones(ArrayList<Union> uniones) {
        this.uniones = uniones;
    }

    public void agregarPareja(Node p) {
        Union nueva = new Union(this, p, true);
        for (int i = 0; i < this.getUniones().size(); i++) {
            Union aux = this.getUniones().get(i);
            aux.setActual(false);
        }
        for (int i = 0; i < p.getUniones().size(); i++) {
            Union aux = p.getUniones().get(i);
            aux.setActual(false);
        }
        this.getUniones().add(nueva);
        p.getUniones().add(nueva);
    }

    public Node getParejaActual() {
        Node ret = null;
        for (int i = 0; i < this.getUniones().size(); i++) {
            Union temp = this.getUniones().get(i);
            if (temp.isActual()) {
                ret = this.getUniones().get(i).getPareja(this);
            }
        }
        return ret;
    }

    public ArrayList<Node> obtenerHijos() {
        ArrayList<Node> lista = new ArrayList<Node>();
        boolean ok = false;
        Union uAux = this.obtenerUnionActual();
        if (uAux != null) {
            for (int j = 0; j < uAux.getListaHijos().size(); j++) {
                lista.add(uAux.getListaHijos().get(j));
            }
        }
        return lista;
    }

    public Union obtenerUnionActual() {
        boolean ok = false;
        Union ret = null;
        for (int i = 0; i < this.getUniones().size() && !ok; i++) {
            Union uAux = this.getUniones().get(i);
            if (uAux.isActual()) {
                ok = true;
                ret = uAux;
            }
        }
        return ret;
    }
    

    public ArrayList<Node> obtenerHermanos() {
        ArrayList<Node> res = new ArrayList<Node>();
        Node padre = this.getPadre();
        if (padre != null) {
            Union uAux = this.getPadre().obtenerUnionActual();
            if (uAux != null) {
                for (int i = 0; i < uAux.getListaHijos().size(); i++) {
                    Node nAux = uAux.getListaHijos().get(i);
                    if (!nAux.equals(this)) {
                        res.add(nAux);
                    }
                }
            }
        }
        return res;
    }

      //Para agregar padres a un nodo se debe elegir una union existente
    public void setearPadres(Union un) {
        Node p1 = un.getP1();
        Node p2 = un.getP2();
        Persona persona1 = (Persona) p1.getMiembro();
        if (persona1.isHombre()) {
            this.setPadre(p1);
            this.setMadre(p2);
        } else {
            this.setPadre(p2);
            this.setMadre(p1);
        }
        un.agregarHijo(this);
    }
    @Override
    public boolean equals(Object o) {
        if (o == null){
            return false;
        }
        return this.getMiembro().equals(((Node) o).getMiembro());
    }
}
