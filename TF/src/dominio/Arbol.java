package dominio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Arbol<Persona> {

    private Node raiz;
    private ArrayList<Node> listaPersonas;

    public Arbol(Node miembro) {
        this.setRaiz(miembro);
        this.setListaPersonas(new ArrayList<Node>());
    }

    public Arbol() {
        this.setListaPersonas(new ArrayList<Node>());
    }

    public ArrayList<Node> getListaPersonas() {
        return listaPersonas;
    }

    public void setListaPersonas(ArrayList<Node> listaPersonas) {
        this.listaPersonas = listaPersonas;
    }

    public Node getRaiz() {
        return raiz;
    }

    public void setRaiz(Node raiz) {
        this.raiz = raiz;
    }

    public String obtenerApellidos(char letra) {
        String res = "";
        char letraM = Character.toUpperCase(letra);
        for (int i = 0; i < listaPersonas.size(); i++) {
            Node nodo = listaPersonas.get(i);
            Persona p = (Persona) nodo.getMiembro();
            String[] apellidoNombre = p.toString().split(" ");
            String apellido = apellidoNombre[0];
            if (letraM == (Character.toUpperCase(apellido.charAt(0)))) {
                res += " " + p.toString() + "\n";
            }
        }
        return res;
    }

    public String ordenarArbolAlf() {
        String res = "";
        for (int i = 65; i < 91; i++) {
            char letra = (char) i;
            String listaXLetra = obtenerApellidos(letra);
            if (!listaXLetra.equals("")) {
                res += letra + "\n" + listaXLetra;
            }
        }
        return res;
    }
}
