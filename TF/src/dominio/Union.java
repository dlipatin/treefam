package dominio;

import java.util.*;

public class Union {

    private Node p1;
    private Node p2;
    private boolean actual;
    private ArrayList<Node> listaHijos;
    private Date fechaInicio;
    private Date fechaFin;

    public Union(Node p1, Node p2, boolean actual, Date fechaI, Date fechaFin) {
        this.setP1(p1);
        this.setP2(p2);
        this.setActual(actual);
        this.setFechaInicio(fechaI);
        this.setFechaFin(fechaFin);
        this.setListaHijos(new ArrayList<Node>());
    }

    public Union(Node p1, Node p2, boolean actual) {
        this.setP1(p1);
        this.setP2(p2);
        this.setActual(actual);
        this.setListaHijos(new ArrayList<Node>());
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public boolean isActual() {
        return actual;
    }

    public void setActual(boolean actual) {
        this.actual = actual;
    }

    public Node getPareja(Node p) {
        Node ret = null;
        if (p.equals(p1)) {
            ret = p2;
        } else {
            ret = p1;
        }
        return ret;
    }

    public void agregarHijo(Node hijo) {
        if (this.getListaHijos().size() < 4) {
            if (p1.getMiembro().isHombre()) {
                hijo.setPadre(p1);
                hijo.setMadre(p2);
            } else {
                hijo.setPadre(p2);
                hijo.setMadre(p1);
            }
            this.getListaHijos().add(hijo);
        }
    }

    public Node getP1() {
        return p1;
    }

    public void setP1(Node p1) {
        this.p1 = p1;
    }

    public Node getP2() {
        return p2;
    }

    public void setP2(Node p2) {
        this.p2 = p2;
    }

    public ArrayList<Node> getListaHijos() {
        return listaHijos;
    }

    public void setListaHijos(ArrayList<Node> listaHijos) {
        this.listaHijos = listaHijos;
    }

    public String listarHijos() {
        String res = "";
        for (int i = 0; i < this.getListaHijos().size(); i++) {
            Persona p = this.getListaHijos().get(i).getMiembro();
            res += p.toString() + "\n";
        }
        return res;
    }
}
