/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

import org.junit.Before;
import dominio.*;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
/**
 *
 * @author Joaquin
 */
public class PruebaArbol {
    
    private Persona principal;
    private Arbol tree;
    private Node nodo;
    
    public PruebaArbol() {
    }
    
    
    @Before
    public void setUp() {
      tree = new Arbol();
      principal = new Persona ("Joaquin", "Perez", 25,2,1994,true,"Uruguay");
      nodo = new Node(principal,tree);
      tree.setRaiz(nodo);
    }
    
    @Test
    public void testGetRaiz(){
        Node resultadoEsperado = nodo;
        Node resultadoObtenido = tree.getRaiz();
        assertEquals(resultadoEsperado,resultadoObtenido);
    }

    @Test
    public void testGetRaizNull(){
        Node resultadoEsperado = null;
        tree.setRaiz(null);
        Node resultadoObtenido = tree.getRaiz();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    @Test
    public void testSetRaiz(){
        Node resultadoEsperado = nodo;
        tree.setRaiz(nodo);
        Node resultadoObtenido = tree.getRaiz();
        assertEquals(resultadoEsperado,resultadoObtenido);
    }
    @Test
    public void testSetRaizNull(){
        Node resultadoEsperado = null;
        tree.setRaiz(null);
        Node resultadoObtenido = tree.getRaiz();
        assertEquals(resultadoEsperado,resultadoObtenido);
    }
    @Test
    public void testGetListaPersonas(){
        ArrayList<Node> resultadoEsperado = new ArrayList<Node>();
        resultadoEsperado.add(nodo);
        ArrayList<Node> resultadoObtenido = tree.getListaPersonas();
        assertEquals(resultadoEsperado,resultadoObtenido);
    }
    
    @Test
    public void testGetListaPersonasVacia(){
        ArrayList<Node> resultadoEsperado = new ArrayList<Node>();
        tree.setListaPersonas(new ArrayList<Node>());
        ArrayList<Node> resultadoObtenido = tree.getListaPersonas();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    @Test
    public void testSetListaPersonas(){
        ArrayList<Node> resultadoEsperado = new ArrayList<Node>();
        resultadoEsperado.add(nodo);
        ArrayList<Node> lista = new ArrayList<Node>();
        lista.add(nodo);
        tree.setListaPersonas(lista);
        ArrayList<Node> resultadoObtenido = tree.getListaPersonas();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testSetListaPersonasVacia(){
        ArrayList<Node> lista = new ArrayList<Node>();
        tree.setListaPersonas(lista);
        ArrayList<Node> resultadoEsperado = new ArrayList<Node>();
        ArrayList<Node> resultadoObtenido = tree.getListaPersonas();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testObtenerApellidos(){
        String resultadoEsperado = " Perez Joaquin\n";
        String resultadoObtenido = tree.obtenerApellidos('P');
        assertEquals(resultadoEsperado,resultadoObtenido);
            
    }
    
    @Test
    public void testObtenerApellidosLetraErronea(){
        String resultadoEsperado = "";
        String resultadoObtenido = tree.obtenerApellidos('A');
        assertEquals(resultadoEsperado,resultadoObtenido);
    }
    
    @Test
    public void testOrdenarArbolAlf(){
        String resultadoEsperado = "P\n Perez Joaquin\n";
        String resultadoObtenido = tree.ordenarArbolAlf();
        assertEquals(resultadoEsperado,resultadoObtenido);
    }
    
    @Test
    public void testOrdenarArbolAlfVacio(){
        tree = new Arbol();
        String resultadoEsperado = "";
        String resultadoObtenido = tree.ordenarArbolAlf();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
}
