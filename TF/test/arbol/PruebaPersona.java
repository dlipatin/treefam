package arbol;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import dominio.*;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.ImageIcon;

public class PruebaPersona {

    private Persona p1, p2, p3;

    public PruebaPersona() {
    }

    @Before
    public void setUp() {
        p1 = new Persona("Daniel", "Lipatin", 11, 2, 94, true, "Uruguayo");
        p2 = new Persona("Carina", "Gonzales", 3, 4, 82, false, "Argentina");
        p3 = new Persona();
    }

    @Test
    public void testGetNombreNoVacio() {
        String resultadoEsperado = "Daniel";
        String resultadoObtenido = p1.getNombre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetNombreVacio() {
        String resultadoEsperado = "No Name";
        String resultadoObtenido = p3.getNombre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetNombre() {
        String resultadoEsperado = "Carlitos";
        p3.setNombre("Carlitos");
        String resultadoObtenido = p3.getNombre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetApellidoNoVacio() {
        String resultadoEsperado = "Gonzales";
        String resultadoObtenido = p2.getApellido();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetApellidoVacio() {
        String resultadoEsperado = "No Last Name";
        String resultadoObtenido = p3.getApellido();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetApellido() {
        String resultadoEsperado = "Arruabarrena";
        p2.setApellido("Arruabarrena");
        String resultadoObtenido = p2.getApellido();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetIsHombreTrue() {
        boolean resultadoObtenido = p1.isHombre();
        assertTrue(resultadoObtenido);
    }

    @Test
    public void testIsHombreFalse() {
        boolean resultadoObtenido = p2.isHombre();
        assertFalse(resultadoObtenido);
    }

    @Test
    public void testSetEsHombre() {
        p3.setEsHombre(true);
        boolean resultadoObtenido = p3.isHombre();
        assertTrue(resultadoObtenido);
    }

    @Test
    public void testSetEsHombreFalse() {
        p3.setEsHombre(false);
        boolean resultadoObtenido = !p3.isHombre();
        assertTrue(resultadoObtenido);
    }

    @Test
    public void testGetFecha() {
        Date resultadoEsperado = new Date(94, 1, 11);
        Date resultadoObtenido = p1.getFecha();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetFecha() {
        Date fecha = new Date(94, 8, 29);
        p1.setFecha(fecha);
        Date resultadoEsperado = new Date(94, 8, 29);
        Date resultadoObtenido = p1.getFecha();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetNacionalidad() {
        String resultadoEsperado = "Uruguayo";
        String resultadoObtenido = p1.getNacionalidad();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetNacionalidadVacia() {
        String resultadoEsperado = "No Nation";
        String resultadoObtenido = p3.getNacionalidad();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetNacionalidad() {
        p3.setNacionalidad("Afgano");
        String resultadoEsperado = "Afgano";
        String resultadoObtenido = p3.getNacionalidad();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetLugarNac() {
        String resultadoEsperado = "Hospital";
        String resultadoObtenido = p1.getLugarNac();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetLugarNac() {
        p1.setLugarNac("Uruguay");
        String resultadoEsperado = "Uruguay";
        String resultadoObtenido = p1.getLugarNac();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetApodo() {
        String resultadoEsperado = "No Surname";
        String resultadoObtenido = p1.getApodo();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetApodo() {
        p1.setApodo("Cangrejo");
        String resultadoEsperado = "Cangrejo";
        String resultadoObtenido = p1.getApodo();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetFotoPerfil() {
        ImageIcon resultadoEsperado = new ImageIcon("src/img/icono.png");
        p1.setFotoPerfil(resultadoEsperado);
        ImageIcon resultadoObtenido = p1.getFotoPerfil();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetFotoPerfil() {
        ImageIcon resultadoEsperado = new ImageIcon("src/img/icono.png");
        p1.setFotoPerfil(resultadoEsperado);
        ImageIcon resultadoObtenido = p1.getFotoPerfil();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testGetListaImagenesVacia(){
        ArrayList<ImageIcon> resultadoEsperado = new ArrayList<ImageIcon>();
        ArrayList<ImageIcon> resultadoObtenido = p1.getListaImagenes();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testGetListaImagenes(){
        ArrayList<ImageIcon> resultadoEsperado = new ArrayList<ImageIcon>();
        resultadoEsperado.add(new ImageIcon("src/img/icono.png"));
        p1.setListaImagenes(resultadoEsperado);
        ArrayList<ImageIcon> resultadoObtenido = p1.getListaImagenes();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testSetListaImagenes(){
        ArrayList<ImageIcon> resultadoEsperado = new ArrayList<ImageIcon>();
        resultadoEsperado.add(new ImageIcon("src/img/icono.png"));
        p1.setListaImagenes(resultadoEsperado);
        ArrayList<ImageIcon> resultadoObtenido = p1.getListaImagenes();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testSetListaImagenesVacio(){
        ArrayList<ImageIcon> resultadoEsperado = new ArrayList<ImageIcon>();
        p1.setListaImagenes(resultadoEsperado);
        ArrayList<ImageIcon> resultadoObtenido = p1.getListaImagenes();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testGetListaDocsVacia(){
        ArrayList<ImageIcon> resultadoEsperado = new ArrayList<ImageIcon>();
        ArrayList<ImageIcon> resultadoObtenido = p1.getListaDocs();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testSetListaDocsVacia(){
        ArrayList<ImageIcon> resultadoEsperado = new ArrayList<ImageIcon>();
        p1.setListaDocs(resultadoEsperado);
        ArrayList<ImageIcon> resultadoObtenido = p1.getListaDocs();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testGetListaDocs(){
        ArrayList<ImageIcon> resultadoEsperado = new ArrayList<ImageIcon>();
        resultadoEsperado.add(new ImageIcon("src/img/icono.png"));
        p1.setListaDocs(resultadoEsperado);
        ArrayList<ImageIcon> resultadoObtenido = p1.getListaDocs();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testSetListaDocs(){
        ArrayList<ImageIcon> resultadoEsperado = new ArrayList<ImageIcon>();
        resultadoEsperado.add(new ImageIcon("src/img/icono.png"));
        p1.setListaDocs(resultadoEsperado);
        ArrayList<ImageIcon> resultadoObtenido = p1.getListaDocs();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testCompareToIguales(){
        Persona pAux = new Persona("Daniel", "Lipatin", 11, 2, 94, true, "Uruguayo");
        boolean resultadoObtenido = p1.compareTo(pAux) == 0;
        assertTrue(resultadoObtenido);
    }
    
    @Test
    public void testCompareToDistintos(){
        boolean resultadoObtenido = p1.compareTo(p2) != 0;
        assertTrue(resultadoObtenido);
    }
    
    @Test
    public void testEquals(){
        Persona pAux = new Persona("Daniel", "Lipatin", 11, 2, 94, true, "Uruguayo");
        boolean resultadoObtenido = p1.equals(pAux);
        assertTrue(resultadoObtenido);
    }
    
    @Test
    public void testNotEquals(){
        boolean resultadoObtenido = p1.equals(p2);
        assertFalse(resultadoObtenido);
    }
}
