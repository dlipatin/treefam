/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import dominio.*;
import java.util.ArrayList;

/**
 *
 * @author Joaquin
 */
public class PruebaNode {

    private Persona principal;
    private Node nodo;
    private Arbol tree;
    private Union union;
    private ArrayList<Node> hnos;
    private Node pareja;
    private Node padre;
    private Node madre;
    private Node nHnoP;

    public PruebaNode() {
    }

    @Before
    public void setUp() {
        tree = new Arbol();
        principal = new Persona("Manuel", "Diaz", 29, 7, 90, true, "Uruguay");
        nodo = new Node(principal, tree);
        Persona padreP = new Persona("Carlos", "Diaz", 10, 10, 70, true, "Uruguay");
        padre = new Node(padreP, tree);
        Persona madreP = new Persona("Maria", "Perez", 9, 3, 72, false, "Uruguay");
        madre = new Node(madreP, tree);
        Persona parejaP = new Persona("Laura", "Basso", 4, 8, 92, false, "Uruguay");
        pareja = new Node(parejaP, tree);
        Persona hnoP = new Persona("Sol", "Diaz", 3,3,94, false, "Uruguay");
        nHnoP = new Node(hnoP, tree);
        hnos = new ArrayList<Node>();
        tree.setRaiz(nodo);
        padre.agregarPareja(madre);
        Union padres = padre.obtenerUnionActual();
        nodo.setearPadres(padres);
        nHnoP.setearPadres(padres);
        hnos.add(nHnoP);
        nodo.agregarPareja(pareja);
        union = nodo.obtenerUnionActual();
        
    }

    @Test
    public void testGetTree() {
        Arbol resultadoEsperado = tree;
        Arbol resultadoObtenido = nodo.getTree();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetTreeNull() {
        Arbol resultadoEsperado = null;
        nodo.setTree(null);
        Arbol resultadoObtenido = nodo.getTree();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetTree() {
        Arbol resultadoEsperado = tree;
        nodo.setTree(tree);
        Arbol resultadoObtenido = nodo.getTree();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetTreeNull() {
        Arbol resultadoEsperado = null;
        nodo.setTree(null);
        Arbol resultadoObtenido = nodo.getTree();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetPadre() {
        Node resultadoEsperado = padre;
        nodo.setPadre(padre);
        Node resultadoObtenido = nodo.getPadre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetPadreNull() {
        Node resultadoEsperado = null;
        nodo.setPadre(null);
        Node resultadoObtenido = nodo.getPadre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetPadre() {
        Node resultadoEsperado = padre;
        nodo.setPadre(padre);
        Node resultadoObtenido = nodo.getPadre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetPadreNull() {
        Node resultadoEsperado = null;
        nodo.setPadre(null);
        Node resultadoObtenido = nodo.getPadre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetMadre() {
        Node resultadoEsperado = madre;
        nodo.setMadre(madre);
        Node resultadoObtenido = nodo.getMadre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetMadreNull() {
        Node resultadoEsperado = null;
        nodo.setMadre(null);
        Node resultadoObtenido = nodo.getMadre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetMadre() {
        Node resultadoEsperado = madre;
        nodo.setMadre(madre);
        Node resultadoObtenido = nodo.getMadre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetMadreNull() {
        Node resultadoEsperado = null;
        nodo.setMadre(null);
        Node resultadoObtenido = nodo.getMadre();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetMiembro() {
        Persona resultadoEsperado = principal;
        Persona resultadoObtenido = nodo.getMiembro();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetMiembroVacio() {
        Persona resultadoEsperado = new Persona();
        nodo.setMiembro(resultadoEsperado);
        Persona resultadoObtenido = nodo.getMiembro();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetMiembro() {
        Persona resultadoEsperado = principal;
        nodo.setMiembro(principal);
        Persona resultadoObtenido = nodo.getMiembro();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetMiembroVacio() {
        Persona resultadoEsperado = new Persona();
        nodo.setMiembro(resultadoEsperado);
        Persona resultadoObtenido = nodo.getMiembro();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetUniones() {
        ArrayList<Union> resultadoEsperado = new ArrayList<Union>();
        resultadoEsperado.add(union);
        ArrayList<Union> resultadoObtenido = nodo.getUniones();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetUnionesVacia() {
        ArrayList<Union> resultadoEsperado = new ArrayList<Union>();
        nodo.setUniones(resultadoEsperado);
        ArrayList<Union> resultadoObtenido = nodo.getUniones();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetUniones() {
        ArrayList<Union> resultadoEsperado = new ArrayList<Union>();
        resultadoEsperado.add(union);
        nodo.setUniones(resultadoEsperado);
        ArrayList<Union> resultadoObtenido = nodo.getUniones();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetUnionesVacia() {
        ArrayList<Union> resultadoEsperado = new ArrayList<Union>();
        nodo.setUniones(resultadoEsperado);
        ArrayList<Union> resultadoObtenido = nodo.getUniones();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testAgregarPareja() {
        Node resultadoEsperado = pareja;
        nodo.agregarPareja(pareja);
        Node resultadoObtenido = nodo.getParejaActual();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetParejaActual() {
        Node resultadoEsperado = pareja;
        Node resultadoObtenido = nodo.getParejaActual();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testGetParejaActualNull(){
        Node nuevo = new Node(principal, tree);
        Node resultadoEsperado = null;
        Node resultadoObtenido = nuevo.getParejaActual();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testObtenerHijos(){
        ArrayList<Node> resultadoEsperado = new ArrayList<Node>();
        Persona p = new Persona("Facundo", "Diaz", 10,10,10,true,"Uruguay");
        Node n = new Node(p,tree);
        n.setearPadres(union);
        resultadoEsperado.add(n);
        ArrayList<Node> resultadoObtenido = nodo.obtenerHijos();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testObtenerHijosVacio(){
        ArrayList<Node> resultadoEsperado = new ArrayList<Node>();
        ArrayList<Node> resultadoObtenido = nodo.obtenerHijos();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testObtenerHermanos(){
        ArrayList<Node> resultadoEsperado = hnos;
        ArrayList<Node> resultadoObtenido = nodo.obtenerHermanos();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testObtenerHermanosVacio(){
        ArrayList<Node> resultadoEsperado = new ArrayList<Node>();
        ArrayList<Node> resultadoObtenido = padre.obtenerHermanos();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testObtenerUnionActual(){
        Union resultadoEsperado = union;
        Union resultadoObtenido = nodo.obtenerUnionActual();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testObtenerUnionActualNull(){
        Union resultadoEsperado = null;
        Union resultadoObtenido = nHnoP.obtenerUnionActual();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testSetearPadres(){
        Node padreEsperado = padre;
        Node madreEsperada = madre;
        nodo.setearPadres(padre.obtenerUnionActual());
        Node padreObtenido = nodo.getPadre();
        Node madreObtenida = nodo.getMadre();
        assertEquals(padreEsperado, padreObtenido);
        assertEquals(madreEsperada, madreObtenida);
    }
    
    @Test
    public void testEqualsIguales(){
        Node nuevo = new Node(new Persona("Manuel", "Diaz", 29, 7, 90, true, "Uruguay"),tree);
        boolean resultadoObtenido= nodo.equals(nuevo);
        assertTrue(resultadoObtenido);
    }
    
    @Test
    public void testEqualsDistintos(){
        boolean resultadoObtenido = nodo.equals(padre);
        assertFalse(resultadoObtenido);
    }
}
