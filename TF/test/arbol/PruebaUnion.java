package arbol;

import dominio.*;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class PruebaUnion {

    private Node n1;
    private Node n2;
    private Arbol tree;
    private Union union;

    public PruebaUnion() {
    }

    @Before
    public void setUp() {
        tree = new Arbol();
        tree.setRaiz(n1);
        n1 = new Node(new Persona("Martin", "Rodriguez", 64, 5, 4, true, "Venezuela"), tree);
        n2 = new Node(new Persona("Carla", "Ramirez", 65, 3, 7, false, "Argentina"), tree);
        union = new Union(n1, n2, true);
        Date fechaInicio = new Date(84, 8, 9);
        Date fechaFin = new Date(99, 3, 3);
        union.setFechaInicio(fechaInicio);
        union.setFechaFin(fechaFin);
    }

    @Test
    public void testGetFechaInicio() {
        Date resultadoEsperado = new Date(84, 8, 9);
        Date resultadoObtenido = union.getFechaInicio();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetFechaIncio() {
        Date resultadoEsperado = new Date(84, 8, 9);
        union.setFechaInicio(resultadoEsperado);
        Date resultadoObtenido = union.getFechaInicio();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetFechaFin() {
        Date resultadoEsperado = new Date(99, 3, 3);
        Date resultadoObtenido = union.getFechaFin();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetFechaFin() {
        Date resultadoEsperado = new Date(80, 5, 4);
        union.setFechaFin(resultadoEsperado);
        Date resultadoObtenido = union.getFechaFin();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetP1() {
        Node resultadoEsperado = n1;
        Node resultadoObtenido = union.getP1();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetP1() {
        Node resultadoEsperado = n2;
        union.setP1(n2);
        Node resultadoObtenido = union.getP1();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetP2() {
        Node resultadoEsperado = n2;
        Node resultadoObtenido = union.getP2();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testSetP2() {
        Node resultadoEsperado = n2;
        union.setP2(n2);
        Node resultadoObtenido = union.getP2();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testIsActualTrue() {
        boolean resultadoObtenido = union.isActual();
        assertTrue(resultadoObtenido);
    }

    @Test
    public void testIsActualFalse() {
        Union unAux = new Union(n1, n2, false);
        boolean resultadoObtenido = unAux.isActual();
        assertFalse(resultadoObtenido);
    }

    @Test
    public void testSetActualFalse() {
        union.setActual(false);
        boolean resultadoObtenido = union.isActual();
        assertFalse(resultadoObtenido);
    }

    @Test
    public void testSetActualTrue() {
        union.setActual(true);
        boolean resultadoObtenido = union.isActual();
        assertTrue(resultadoObtenido);
    }

    @Test
    public void testGetPareja() {
        Node resultadoEsperado = n1;
        Node resultadoObtenido = union.getPareja(n2);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testGetPareja2() {
        Node resultadoEsperado = n2;
        Node resultadoObtenido = union.getPareja(n1);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testAgregarHijo() {
        Node hijo = new Node(new Persona("Carlos", "Gutierrez", 10, 10, 94, true, "Uruguay"), tree);
        Node resultadoEsperado = hijo;
        union.agregarHijo(hijo);
        int indx = union.getListaHijos().size() - 1;
        Node resultadoObtenido = union.getListaHijos().get(indx);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testAgregarHijoListaLlena() {
        Node hijo1 = new Node(new Persona("Carlos", "Gutierrez", 10, 10, 94, true, "Uruguay"), tree);
        Node hijo2 = new Node(new Persona("Marta", "Gutierrez", 10, 5, 94, false, "Uruguay"), tree);
        Node hijo3 = new Node(new Persona("Elsa", "Gutierrez", 7, 8, 90, false, "Uruguay"), tree);
        Node hijo4 = new Node(new Persona("Teo", "Gutierrez", 3, 10, 90, true, "Uruguay"), tree);
        Node hijo5 = new Node(new Persona("Leo", "Gutierrez", 3, 6, 93, true, "Uruguay"), tree);
        union.agregarHijo(hijo1);
        union.agregarHijo(hijo2);
        union.agregarHijo(hijo3);
        union.agregarHijo(hijo4);
        union.agregarHijo(hijo5);
        int indx = union.getListaHijos().size();
        boolean resultadoObtenido = (indx <= 4);
        assertTrue(resultadoObtenido);
    }

    @Test
    public void testListarHijos() {
        Node hijo1 = new Node(new Persona("Carlos", "Gutierrez", 10, 10, 94, true, "Uruguay"), tree);
        Node hijo2 = new Node(new Persona("Marta", "Gutierrez", 10, 5, 94, false, "Uruguay"), tree);
        union.agregarHijo(hijo1);
        union.agregarHijo(hijo2);
        String resultadoEsperado = "Gutierrez Carlos\nGutierrez Marta\n";
        String resultadoObtenido = union.listarHijos();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void testListarHijosVacio(){
        String resultadoEsperado = "";
        String resultadoObtenido = union.listarHijos();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
}
